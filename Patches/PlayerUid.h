#pragma once

#include <cstdint>

namespace Patches
{
	namespace PlayerUid
	{
		void ApplyAll();
		uint64_t Get();
	}
}